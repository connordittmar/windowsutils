from platform   import system as system_name  # Returns the system/OS name
from subprocess import call   as system_call  # Execute a shell command

# Program to Check Network Connections


hosts_team = {'team_router' : '192.168.1.1',
              'onboard_odroid' : '192.168.1.40',
              'pg_camera' : '192.168.1.60',
              'axis_camera' : '192.168.1.80',
              'ground_odroid' : '192.168.1.100' }

hosts_judge = {'judges_router' : '10.10.130.1',
               'judges_server' : '10.10.130.2'}

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Ping command count option as function of OS
    param = '-n' if system_name().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    # Pinging
    return system_call(command) == 0

def is_connected(hosts_dict):
    # Iterate over team hosts dictionary and ping each
    all_connected = True
    for key, value in hosts_dict.iteritems():
        if ping(value):
            print(key," is connected")
        else:
            print(key, " is not connected")
            all_connected = False
    return all_connected

if __name__ == "__main__":
    print("Testing Team Connections...")
    print(" ")
    team_connect = is_connected(hosts_team)
    print(" ")
    print("Testing Judge Connections...")
    print(" ")
    judges_connect = is_connected(hosts_judge)
    print(" ")
    print("--------------------------")
    print(" ")
    if team_connect:
        print("Team Network is Online.")
    else:
        print("Team Network has a problem, look to above output to debug.")
    if judges_connect:
        print("Judges Network is Online.")
    else:
        print("Team Network has a problem, look to above output to debug.")
